Tech Stack:

JDK 17
Spring Data JPA
Spring Boot
H2
Docker
Maven
Git

1-) First, a bank account is created

curl --location --request POST 'localhost:8080' \
--header 'Content-Type: application/json' \
--data '{
"owner" : "bugra celik",
"accountNumber" : "17328921",
"balance" : 12022

}'

2-)To carry out bank transactions, we need to write the transaction we will do in the type section and then write the transaction amount.

Example of Deposit Transaction

curl --location --request POST 'localhost:8080/17328921' \
--header 'Content-Type: application/json' \
--data '{
"type": "DEPOSIT_TRANSACTION",
"amount": 10000
}'

Example of Withdrawal Transaction

curl --location --request POST 'localhost:8080/17328921' \
--header 'Content-Type: application/json' \
--data '{
"type": "WITHDRAWAL_TRANSACTION",
"amount": 10000
}'

3-)The following curl is run to view the transactions made by the user according to the relevant account number.

curl --location 'localhost:8080/17328921' \
--data ''


4-) Click to view all transactions from h2 database's web UI : http://localhost:8080/h2-console/


Dockerize Application Steps
1-)Build jar with Maven
2-)docker build -t simple-banking.jar . (Create image)
3-)docker run – p 8090:8080 simple-banking.jar


NOTE : 
When a new transaction needs to be added to the system, the new class that will come into the
com.eteration.banking.app.banking.model package must extend the Transaction class, and the definition of the new class
must be specified in @JsonSubTypes.Type above the Transaction class. Thanks to this implementation, we can safely execute 
all transactions from a single endpoint.
Enpoint -> @PutMapping("{accountNumber}")
public ResponseEntity<UUID> transactionProcess(@Valid @RequestBody Transaction transaction, @PathVariable String accountNumber) {
UUID approvalCode = transactionService.bankAccountTransactionFlow(transaction, accountNumber);
return new ResponseEntity<>(approvalCode, HttpStatus.OK);
}

It only accepts the abstract Transaction class, and any undefined Transaction is blocked by Spring before reaching
this endpoint. In this way, it is both safe and we can define the transactions we want to perform in a new transaction 
by overriding the transaction method in the new class without changing the previously written clean codes.
