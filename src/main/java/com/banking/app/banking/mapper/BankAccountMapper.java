package com.banking.app.banking.mapper;


import com.banking.app.banking.dto.BankAccountResponseDto;
import com.banking.app.banking.model.BankAccount;
import com.banking.app.banking.dto.BankAccountRequestDto;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.time.LocalDateTime;
import java.util.UUID;

@Mapper
public interface BankAccountMapper {

    BankAccountMapper MAPPER = Mappers.getMapper(BankAccountMapper.class);

    BankAccount mapToBankAccountDto(BankAccountRequestDto bankAccountRequestDto);

    BankAccountResponseDto mapToBankAccountResponseDto(BankAccount bankAccount);


}
