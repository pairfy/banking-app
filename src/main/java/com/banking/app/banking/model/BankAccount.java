package com.banking.app.banking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;


@Data
@Entity
@Table(name = "bank_account")
public class BankAccount {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @JsonIgnore
    private String id;
    @Column(nullable = false)
    private String owner;
    private String gender;
    private LocalDate birthDate;
    private String gsm;
    private String accountNumber;
    private double balance;
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createDate;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Column(updatable = false, insertable = false)
    private List<TransactionDetails> transactions;
}
