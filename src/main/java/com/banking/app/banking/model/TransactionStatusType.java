package com.banking.app.banking.model;

public enum TransactionStatusType {
    DEPOSIT_TRANSACTION, WITHDRAWAL_TRANSACTION;
}
