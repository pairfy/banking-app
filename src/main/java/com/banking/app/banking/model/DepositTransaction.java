package com.banking.app.banking.model;


import com.banking.app.banking.exception.BadRequestException;

public class DepositTransaction extends Transaction {
    @Override
    public void transaction(BankAccount bankAccount, double amount) {
        double credit = credit(bankAccount.getBalance(), amount);
        bankAccount.setBalance(credit);
    }

    private double credit(double accountCurrentBalance, double amount) {
        if (amount == 0) {
            throw new BadRequestException("Balance top-up cannot be performed with 0 balance.");
        }

        return accountCurrentBalance + amount;
    }
}
