package com.banking.app.banking.model;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = DepositTransaction.class, name = "DEPOSIT_TRANSACTION"),
        @JsonSubTypes.Type(value = WithdrawalTransaction.class, name = "WITHDRAWAL_TRANSACTION")
})
@Data
@ToString
public abstract class Transaction {
    private TransactionStatusType type;
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime date;
    @Min(value = 0, message = "The transaction amount cannot be less than 0.")
    private Double amount;

    public abstract void transaction(BankAccount bankAccount, double amount);
}
