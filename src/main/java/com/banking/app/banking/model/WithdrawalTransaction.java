package com.banking.app.banking.model;


import com.banking.app.banking.exception.InsufficientBalanceException;
import com.banking.app.banking.exception.BadRequestException;

public class WithdrawalTransaction extends Transaction {
    @Override
    public void transaction(BankAccount bankAccount, double amount) {
        double debit = debit(bankAccount.getBalance(), amount);
        bankAccount.setBalance(debit);
    }

    private double debit(double accountCurrentBalance, double amount) {
        if (amount == 0) {
            throw new BadRequestException("The value of the operation performed cannot be 0");
        }

        if (amount > accountCurrentBalance) {
            throw new InsufficientBalanceException("account balance is insufficient for this transaction");
        }

        return accountCurrentBalance - amount;
    }
}


