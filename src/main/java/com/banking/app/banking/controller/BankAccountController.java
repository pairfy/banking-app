package com.banking.app.banking.controller;

import com.banking.app.banking.dto.BankAccountRequestDto;
import com.banking.app.banking.dto.BankAccountResponseDto;
import com.banking.app.banking.model.Transaction;
import com.banking.app.banking.service.IBankAccountService;
import com.banking.app.banking.service.ITransactionService;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("bank-account/v1")
@RateLimiter(name = "invitationRateLimit")
@RequiredArgsConstructor
public class BankAccountController {

    private final IBankAccountService bankAccountService;
    private final ITransactionService transactionService;

    @GetMapping
    public ResponseEntity<BankAccountResponseDto> getAccountInformation(@RequestParam @NonNull String accountNumber) {
        BankAccountResponseDto bankAccountResponseDto = bankAccountService.findByAccountNumber(accountNumber);
        return new ResponseEntity<>(bankAccountResponseDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<BankAccountResponseDto> createBankAccount(@RequestBody BankAccountRequestDto bankAccountRequestDto) {
        BankAccountResponseDto bankAccountResponseDto = bankAccountService.addAccount(bankAccountRequestDto);
        return new ResponseEntity<>(bankAccountResponseDto, HttpStatus.CREATED);
    }

    @PostMapping("{accountNumber}")
    public ResponseEntity<UUID> transactionProcess(@RequestBody Transaction transaction, @PathVariable String accountNumber) {
        UUID approvalCode = transactionService.bankAccountTransactionFlow(transaction, accountNumber);
        return new ResponseEntity<>(approvalCode, HttpStatus.OK);
    }

    //jwt ile sadece kullanıcının transactionları görebildigi endpoint geliştirmesi..

    //endpoint validasyonu..
}
