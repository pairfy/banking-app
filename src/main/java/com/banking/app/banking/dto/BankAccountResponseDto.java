package com.banking.app.banking.dto;

import com.banking.app.banking.model.Transaction;
import com.banking.app.banking.model.TransactionDetails;

import java.util.List;

public record BankAccountResponseDto(String owner, String gender, String accountNumber,
                                     Double balance, List<TransactionDetails> transaction) {
}
