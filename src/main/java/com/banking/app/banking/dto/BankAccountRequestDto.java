package com.banking.app.banking.dto;

import java.time.LocalDate;

public record BankAccountRequestDto(String owner, String gender, LocalDate birthDate, String gsm) {
    public BankAccountRequestDto {
        if (birthDate.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Geçerli bir tarih girilmelidir.");
        }
    }
}
