package com.banking.app.banking.configuration;

import com.banking.app.banking.service.RateLimiterService;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class GlobalConfiguration {

    private final RateLimiterRegistry rateLimiterRegistry;
    private final RateLimiterService rateLimiterService;
    public static final String INVITATION_RATE_LIMIT = "invitationRateLimit";

    @Bean(name = "rateLimiterConfig")
    public RateLimiter rateLimiterConfig() {
        return rateLimiterRegistry.rateLimiter(INVITATION_RATE_LIMIT, rateLimiterService.rateLimiterConfig());
    }
}
