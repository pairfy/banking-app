package com.banking.app.banking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends ResponseStatusException {
    public ResourceNotFoundException(@Nullable String text) {
        super(HttpStatus.NOT_FOUND, text);
    }
}
