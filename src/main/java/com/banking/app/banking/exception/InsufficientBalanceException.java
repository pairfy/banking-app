package com.banking.app.banking.exception;


import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InsufficientBalanceException extends ResponseStatusException {
    public InsufficientBalanceException(@Nullable String text) {
        super(HttpStatus.BAD_REQUEST, text);
    }
}
