package com.banking.app.banking.service.impl;


import com.banking.app.banking.dto.BankAccountResponseDto;
import com.banking.app.banking.exception.BadRequestException;
import com.banking.app.banking.mapper.BankAccountMapper;
import com.banking.app.banking.model.BankAccount;
import com.banking.app.banking.dto.BankAccountRequestDto;
import com.banking.app.banking.repository.BankAccountRepository;
import com.banking.app.banking.service.IBankAccountService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements IBankAccountService {

    private static final Logger logger = LogManager.getLogger(AccountServiceImpl.class);

    private final BankAccountRepository bankAccountRepository;

    @PostConstruct
    public void post() {
        BankAccount bankAccount = new BankAccount();
        bankAccount.setOwner("bugra");
        bankAccount.setGender("erkek");
        bankAccount.setAccountNumber("b90c9340-c2bb-4cb8-809f-82c08d4aa510");
        bankAccount.setCreateDate(LocalDateTime.now());
        bankAccountRepository.save(bankAccount);
    }

    @Override
    @Transactional
    public BankAccountResponseDto addAccount(BankAccountRequestDto bankAccountRequestDto) throws BadRequestException {
        BankAccount bankAccount = BankAccountMapper.MAPPER.mapToBankAccountDto(bankAccountRequestDto);
        setAccountNumberAndCreateDate(bankAccount);
        BankAccount savedBankAccount = bankAccountRepository.save(bankAccount);
        BankAccountResponseDto bankAccountResponseDto = BankAccountMapper.MAPPER.mapToBankAccountResponseDto(savedBankAccount);
        logger.info(bankAccountRequestDto + "created successfully");
        return bankAccountResponseDto;
    }

    @Override
    public BankAccountResponseDto findByAccountNumber(String accountNumber) {
        if (Objects.isNull(accountNumber)) {
            throw new BadRequestException("Account number can not be null or empty");
        }

        BankAccount bankAccount = bankAccountRepository.findByAccountNumber(accountNumber)
                .orElseThrow(() -> new BadRequestException("There is no bank account with this account number"));


        return BankAccountMapper.MAPPER.mapToBankAccountResponseDto(bankAccount);
    }

    @Override
    public void updateBankAccount(BankAccount bankAccount) {
        bankAccountRepository.save(bankAccount);
    }

    private void setAccountNumberAndCreateDate(BankAccount bankAccount) {
        bankAccount.setCreateDate(LocalDateTime.now());
        //bankAccount.setAccountNumber(UUID.randomUUID().toString());
        bankAccount.setAccountNumber("b90c9340-c2bb-4cb8-809f-82c08d4aa510");
    }
}
