package com.banking.app.banking.service;

import io.github.resilience4j.ratelimiter.RateLimiterConfig;
import io.github.resilience4j.ratelimiter.RateLimiterRegistry;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
@RequiredArgsConstructor
public class RateLimiterService {

    public static final String INVITATION_RATE_LIMIT = "invitationRateLimit";

    private final RateLimiterRegistry rateLimiterRegistry;

    @EventListener(ApplicationReadyEvent.class)
    public void initiate() {
        initializeRateLimiterEventHandler();
    }

    public void initializeRateLimiterEventHandler() {
        rateLimiterRegistry.rateLimiter(INVITATION_RATE_LIMIT)
                .getEventPublisher()
                .onFailure(event -> {
                    throw new RuntimeException("Too Many Requests");
                });
    }

    public void updateRateLimiter() {
        //it would be proxy of scheduled job and connect with redis because of updatable of runtime
        try {
            rateLimiterRegistry.replace(INVITATION_RATE_LIMIT, rateLimiterRegistry.rateLimiter(INVITATION_RATE_LIMIT, rateLimiterConfig()));
        } catch (Exception ex) {

        }
    }

    public RateLimiterConfig rateLimiterConfig() {
        return RateLimiterConfig.custom()
                .limitForPeriod(1000)
                .limitRefreshPeriod(Duration.ofSeconds(10l))
                .timeoutDuration(Duration.ofSeconds(4l))
                .build();
    }
}


