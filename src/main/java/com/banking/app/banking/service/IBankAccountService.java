package com.banking.app.banking.service;

import com.banking.app.banking.dto.BankAccountRequestDto;
import com.banking.app.banking.dto.BankAccountResponseDto;
import com.banking.app.banking.model.BankAccount;
import com.banking.app.banking.exception.BadRequestException;

public interface IBankAccountService {

    BankAccountResponseDto addAccount(BankAccountRequestDto bankAccountRequestDto) throws BadRequestException;

    BankAccountResponseDto findByAccountNumber(String accountNumber);

    void updateBankAccount(BankAccount bankAccount);
}
