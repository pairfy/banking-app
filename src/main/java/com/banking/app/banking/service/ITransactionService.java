package com.banking.app.banking.service;

import com.banking.app.banking.model.Transaction;

import java.util.UUID;

public interface ITransactionService {
    UUID bankAccountTransactionFlow(Transaction transaction, String accountNumber);
}
