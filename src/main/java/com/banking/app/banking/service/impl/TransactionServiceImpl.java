package com.banking.app.banking.service.impl;

import com.banking.app.banking.exception.ResourceNotFoundException;
import com.banking.app.banking.model.BankAccount;
import com.banking.app.banking.model.Transaction;
import com.banking.app.banking.model.TransactionDetails;
import com.banking.app.banking.repository.BankAccountRepository;
import com.banking.app.banking.service.IBankAccountService;
import com.banking.app.banking.service.ITransactionService;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements ITransactionService {

    private final BankAccountRepository bankAccountRepository;
    private final IBankAccountService bankAccountService;
    private static Integer count = 0;

    @Override
    @Transactional
    public UUID bankAccountTransactionFlow(Transaction transaction, String accountNumber) {
        BankAccount bankAccount = findByAccountNumber(accountNumber);
        transaction.transaction(bankAccount, transaction.getAmount());
        TransactionDetails transactionDetails = getTransactionDetails(transaction);
        bankAccount.getTransactions().add(transactionDetails);
        bankAccountService.updateBankAccount(bankAccount);
        return transactionDetails.getApprovalCode();
    }


    //retry mekanızması eklenmeli
    @Retry(name = "retryApi", fallbackMethod = "fallbackAfterRetry")
    private BankAccount findByAccountNumber(String accountNumber) {
        if (count == 3) {
            return bankAccountRepository.findByAccountNumber(accountNumber)
                    .orElseThrow(() -> new ResourceNotFoundException("Account number not exist at DB"));
        }
        System.out.println("retry denemesi : " + ++count);
        throw new RuntimeException("deneme");
    }

    public String fallbackAfterRetry(Exception ex) {
        return "all retries have exhausted";
    }

    private TransactionDetails getTransactionDetails(Transaction transaction) {
        TransactionDetails transactionDetails = new TransactionDetails();
        transactionDetails.setType(transaction.getType());
        transactionDetails.setAmount(transaction.getAmount());
        transactionDetails.setApprovalCode(UUID.randomUUID());
        return transactionDetails;
    }
}
