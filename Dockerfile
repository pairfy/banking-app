FROM openjdk:17
EXPOSE 8080
ADD target/simple-banking.jar simple-banking.jar
ENTRYPOINT ["java", "-jar", "simple-banking.jar"]